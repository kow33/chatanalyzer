package com.example

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.ktor.application.Application



val Application.BD_SERVER_PORT get() = environment.config.property("ktor.deployment.db_server_port").getString().toInt()

val Application.BD_SERVER_HOST get() = environment.config.property("ktor.deployment.db_server_host").getString()

val Application.BD_SERVER_URL get()="http://$BD_SERVER_HOST:$BD_SERVER_PORT"

const val urlPathGetInfoProfessors="v1/info/professors"

const val urlPathGetProfessors="v1/professors"

const val urlPathGetInfoJokes="v1/info/jokes"

const val urlPathGetJokes="v1/jokes"

const val ProfessorLevensteinDistance=3

const val JokesLevensteinDistance=3

const val DaysLevensteinDistance=3


const val pathToProfessorKeyWords="resources/professorKeyWords.txt"

const val pathToJokeKeyWords="resources/jokeKeyWords.txt"

const val pathToExceptionWords="resources/exceptionWords.txt"

const val pathToJokeThemeKeyWords="resources/jokeThemeKeyWords.txt"

const val pathToDayOfWeekKeyEngWords="resources/dayOfWeekKeyEngWords.txt"

const val pathToDayOfWeekKeyRusWords="resources/dayOfWeekKeyRusWords.txt"



//TODO Обобщенка не работает, читать доки!!!
fun deserializationArrayFromGsonProfessor(json:String): List<Professor>{
    val listType = object : TypeToken<List<Professor>>(){}.type
    return Gson().fromJson<List<Professor>>(json,listType)
}

fun deserializationArrayFromGsonJoke(json:String): List<Joke>{
    val listType = object : TypeToken<List<Joke>>(){}.type
    return Gson().fromJson<List<Joke>>(json,listType)
}